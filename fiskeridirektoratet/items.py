# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FartoyregItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    source = scrapy.Field()
    baat_name = scrapy.Field()
    baat_regmerke =scrapy.Field()
    baat_radio = scrapy.Field()
    
    baat_lengde = scrapy.Field()
    baat_bredde = scrapy.Field()
    baat_art = scrapy.Field()
    baat_skrogmateriale = scrapy.Field()
    baat_motor_hk = scrapy.Field()
    baat_tonnasje_1969 = scrapy.Field()
    baat_tonnasje_annen = scrapy.Field()
    baat_byggaar = scrapy.Field()

    baat_kommune = scrapy.Field()
    baat_fylke = scrapy.Field()

    eier_orgnr = scrapy.Field()
    eier_navn = scrapy.Field()
    eier_orgform = scrapy.Field()
    eier_fiskemantall = scrapy.Field()
    eier_postadresse = scrapy.Field()
    eier_postnr_sted = scrapy.Field()
    eier_geo = scrapy.Field()
    
    baat_har_aksjonaerer = scrapy.Field()
    baat_har_kvoter = scrapy.Field()
    baat_har_konsesjoner = scrapy.Field()
    baat_har_fangster = scrapy.Field()
    baat_aksjonaerer = scrapy.Field()
    baat_konsesjoner = scrapy.Field()
    baat_kvoter = scrapy.Field()
    baat_fangster = scrapy.Field()
    
    
    
    pass

class AksjonaerItem(scrapy.Item):
    eier_orgnr = scrapy.Field()
    eier_navn = scrapy.Field()
    eier_orgform = scrapy.Field()
    eier_fiskemantall = scrapy.Field()
    eier_postadresse = scrapy.Field()
    eier_postnr_sted = scrapy.Field()
    eier_geo = scrapy.Field()
    
    baat_kvoter = scrapy.Field()
    #konsesjon_type = scrapy.Field()
    #konsesjon_kvote = scrapy.Field()
    #kvote_fiskeslag = scrapy.Field()
    #kvote_omraade = scrapy.Field()
    #kvote_redskap = scrapy.Field()
    #kvote_tonn = scrapy.Field()
    pass

class KvoterItem(scrapy.Item):
    #source = scrapy.Field()
    kvote_id = scrapy.Field()
    baat_regmerke = scrapy.Field()
    kvote_fiskeslag = scrapy.Field()
    kvote_omraade = scrapy.Field()
    kvote_redskap = scrapy.Field()
    kvote_tonn = scrapy.Field()
    pass
    
class FangstItem(scrapy.Item):
    source = scrapy.Field()
    fangst_id = scrapy.Field()
    baat_regmerke = scrapy.Field()
    fangst_fiskeslag = scrapy.Field()
    fangst_omraade = scrapy.Field()
    fangst_redskap = scrapy.Field()
    fangst_tonn = scrapy.Field()
    fangst_landingsdato = scrapy.Field()


    pass

class FiskerimantalletItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()
    fodt = scrapy.Field()
    fylke = scrapy.Field()
    kommune = scrapy.Field()
    inndato = scrapy.Field()
    blad = scrapy.Field()
    liste = scrapy.Field()
    disp = scrapy.Field()
    utdato = scrapy.Field()
    aarsak = scrapy.Field()
    source = scrapy.Field()

    pass

class FiskekjoperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    orgnr = scrapy.Field()
    navn =scrapy.Field()
    ansvarlig = scrapy.Field()
    leder = scrapy.Field()
    gateadresse = scrapy.Field()
    postnr = scrapy.Field()
    telefon = scrapy.Field()
    telefax = scrapy.Field()
    inndato = scrapy.Field()
    Utdato = scrapy.Field()
    oppdatert = scrapy.Field()
    status = scrapy.Field()
    statuskode = scrapy.Field()
    kommentar = scrapy.Field()
    salgslag = scrapy.Field()
    source = scrapy.Field()

    pass