# -*- coding: utf-8 -*-
import scrapy
import pprint
import pickle
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import re
from scrapy.shell import inspect_response
from scrapy.selector import Selector
from urlparse import urljoin
from fiskeridirektoratet.items import FartoyregItem, KvoterItem


class FiskekjopereSpider(scrapy.Spider):
    name = "fartoyregisteret"
    allowed_domains = ["fiskeridir.no"]
    start_urls = ['http://www.fiskeridir.no/register/fartoyreg/?m=frtyoppl&f=1']

    
    rules = (
       
        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(LinkExtractor(restrict_xpaths='//table[2]/tbody/tr/td/a'), follow=True, callback='parse'),)
    
    
    def parse(self, response):
        print ('Filling out form')
        # an list of fiskeridirektoratets acronyms for counties used in the HTTP POST request form
        fylker = {'F':'Finnmark','T':'Troms','N':'Nordland','NT':'Nord-Trøndelag','ST':'Sør-Trøndelag','M':'Møre og Romsdal','SF':'Sogn og Fjordane','H':'Hordaland','R':'Rogaland','VA':'Vest-Agder','AA':'Aust-Agder','TK':'Telemark','V':'Vestfold','BD':'Buskerud','OP':'Oppland','HE':'Hedmark','O':'Oslo','A':'Akershus','Ø':'Østfold'}

        
        # Loop thru the list av counties and fill out the form and parsing one county at the time
        for key in fylker:
            print ('fetching fylke:', fylker[key])
            yield scrapy.FormRequest.from_response(response,
                formdata={'fylke': key}, callback=self.parse_list, meta={'fylke': fylker[key]})
                #

    def parse_list(self, response):
        #inspect_response(response, self)
        ## parsing all links/boats for the county
        #Getting the fylke as an variable passed thru from the previus form stage
        fylke = response.meta.get('fylke')

        print ('Getting links')

        #Extracting all links to all boats
        links = response.selector.xpath('//table[2]//td[1]/a/@href').extract()
       
        #with open('kommuner.txt', 'rb') as handle:
         #   kommuneliste = pickle.loads(handle.read())

        i=0 
        #looping thru all links to all boats and sending each boat-page to the class parse_item
        for link in links:
            i+=1

            #if i<30:
            if 'http://' not in link:
                          link = urljoin(response.url, link)
                          link = link.encode('WINDOWS_1252')
                          print(fylke,' : ',link)
                          yield scrapy.Request(link,callback=self.parse_item, meta={'fylke': fylke})
        pass    
            

    def parse_item(self, response):
        # Parsing a single boat-page to get all the attributtes for the boat
        
        #inspect_response(response, self)
        # Declearing the boat object imported from the items.py file
        item = FartoyregItem()
        # Using xpath to get values to the variables

        # ******** Boat info section ***************************************************************************************************************
        #*****************************************************************************************************************************************
        item['baat_regmerke'] =  response.xpath('//table[2]/tr[2]/td[2]/text()').extract_first() 
        item['baat_radio'] =  response.xpath('//table[2]/tr[2]/td[4]/text()').extract_first() 
        item['baat_name'] =  response.xpath('//table[2]/tr[2]/td[6]/text()').extract_first()

        item['baat_lengde'] =  response.xpath('//table[2]/tr[4]/td[2]/text()').extract_first()
        item['baat_art'] =  response.xpath('//table[2]/tr[4]/td[4]/text()').extract_first()
        item['baat_motor_hk'] =  response.xpath('//table[2]/tr[4]/td[6]/text()').extract_first()

        item['baat_skrogmateriale'] = response.xpath('//table[2]/tr[5]/td[4]/text()').extract_first()
        item['baat_byggaar'] = response.xpath('//table[2]/tr[6]/td[4]/text()').extract_first()

        item['baat_tonnasje_1969'] = response.xpath('//table[2]/tr[7]/td[2]/text()').extract_first()
        item['baat_tonnasje_annen'] = response.xpath('//table[2]/tr[8]/td[2]/text()').extract_first()

        item['eier_orgnr'] =  response.xpath('//table[3]/tr[2]/td[2]/text()').extract_first()
        item['eier_navn'] =  response.xpath('//table[3]/tr[2]/td[4]/text()').extract_first()
        item['eier_fiskemantall'] =  response.xpath('//table[3]/tr[2]/td[6]/text()').extract_first()
        item['eier_orgform'] =  response.xpath('//table[3]/tr[3]/td[2]/text()').extract_first()
        item['eier_postadresse'] =  response.xpath('//table[3]/tr[3]/td[4]/text()').extract_first()
        item['eier_postnr_sted'] =  response.xpath('//table[3]/tr[4]/td[4]/text()').extract_first()
        
        item['source'] = response.url

        # ****** Adding geo information to the boat description *************************************************************************************
        #********************************************************************************************************************************************

        # Adding a concatenated geo field for easy geoencodeing
        item['eier_geo'] = item['eier_postadresse'] + ", " +item['eier_postnr_sted'] + ", Norway"

        # Adding county fetched from the first stage in the prosess where county is used in the form submit
        item['baat_fylke'] = response.meta.get('fylke')


        # Adding kommune acronym extracted from the regmerke. the last 1 or 2 letters in the regmerke is an acronym representing the kommune
        kommune = str(item['baat_regmerke'])
        m = re.search('.{1,2}\d*(\D*)$',kommune)
        #print.m.group(1)
        #print.m.group(1)
        if m:

            item['baat_kommune'] = m.group(1)
        

        #******************************************************************************************************************************************
        # *********** End of adding Geo information ***********************************************************************************************

        

        regmerke = response.xpath('//table[2]/tr[2]/td[2]/text()').extract_first()

        #****************************************** Adding kvoter ************************************************
        #This section is adding kvoter to the boat description as a nested structure
        kvoter = []
        item['baat_kvoter'] = {}
        rows = response.xpath('//table[6]//tr[position()>2]')
        i = 0
        if rows:
            item['baat_har_kvoter'] = "true"
            for row in rows:
                kvote = {}
                #print row.xpath('td')
                i += 1
                istr = str(i)
           
                #i =  regmerke
                kvote['kvote_regmerke'] = regmerke
                kvote['kvote_nr'] = str(i)
                kvote['kvote_fiskeslag'] =  row.xpath('./td[1]/text()').extract_first()
                kvote['kvote_omraade'] =  row.xpath('./td[2]/text()').extract_first()
                kvote['kvote_redskap'] =  row.xpath('./td[3]/text()').extract_first()
                kvote['kvote_tonn'] =  row.xpath('./td[4]/text()').extract_first()
                #item['source'] = response.url
                #return kvote
                kvoter.append(kvote)
        else:
            item['baat_har_kvoter'] = "false"

        
        
        item['baat_kvoter'] = kvoter

        # ****************** End of adding kvoter ***************************************

        # ****************** Adding aksjonærer **************************************************
        aksjonaerer = []
        item['baat_aksjonaerer'] = {}
        rows = response.xpath('//table[4]//tr[position()>2]')
        i = 0
        if rows:
            item['baat_har_aksjonaerer'] = "true"
            for row in rows:
                aksjonaer = {}
                #print row.xpath('td')
                i += 1
                istr = str(i)
           
                #i =  regmerke
                aksjonaer['aksjonaer_regmerke'] = regmerke
                aksjonaer['aksjonaer_nr'] = str(i)
                aksjonaer['aksjonaer_id'] = row.xpath('./td[1]/text()').extract_first()
                aksjonaer['aksjonaer_navn'] =  row.xpath('./td[2]/text()').extract_first()
                aksjonaer['aksjonaer_andel'] =  row.xpath('./td[3]/text()').extract_first()
                aksjonaer['aksjonaer_fiskerimantall'] =  row.xpath('./td[4]/text()').extract_first()
                #item['source'] = response.url
                #return kvote
                aksjonaerer.append(aksjonaer)
        else:
            item['baat_har_aksjonaerer'] = "false"

        
        
        item['baat_aksjonaerer'] = aksjonaerer
        #******************* End of adding aksjonærer *******************************************

        # ****************** Adding Konsesjoner ****************************************
       

        konsesjoner = []
        item['baat_konsesjoner'] = {}
        rows = response.xpath('//table[5]//tr[position()>2]')
        i = 0
        if rows:
            item['baat_har_konsesjoner'] = "true"
            for row in rows:
                konsesjon = {}
                #print row.xpath('td')
                i += 1
                istr = str(i)
           
                #i =  regmerke
                konsesjon['konsesjon_regmerke'] = regmerke
                konsesjon['konsesjon_nr'] = str(i)
                konsesjon['konsesjon_type'] =  row.xpath('./td[1]/text()').extract_first()
                konsesjon['konsesjon_konsesjon'] =  row.xpath('./td[2]/text()').extract_first()
                #input = row.xpath('./td[2]/text()').extract_first()
                #output = input.split()
                #mengde = re.sub('[\(\)]', '', output[1])
                #konsesjon['konsesjon_benevnelse'] = output[0]
                #konsesjon['konsesjon_storrelse'] = mengde               

               
               
                konsesjoner.append(konsesjon)
        else:
            item['baat_har_konsesjoner'] = "false"

        
        
        item['baat_konsesjoner'] = konsesjoner    





        # ******************* Adding fangster ******************************************
        

        fangster = []
        item['baat_fangster'] = {}
        rows = response.xpath('//table[7]//tr[position()>2]')
        i = 0
        if rows:
            item['baat_har_fangster'] = "true"
            for row in rows:
                fangst = {}
                #print row.xpath('td')
                i += 1
                istr = str(i)
           
                #i =  regmerke
                fangst['fangst_regmerke'] = regmerke
                fangst['fangst_nr'] = str(i)
                fangst['fangst_fiskeslag'] =  row.xpath('./td[1]/text()').extract_first()
                fangst['fangst_omraade'] =  row.xpath('./td[2]/text()').extract_first()
                fangst['fangst_redskap'] =  row.xpath('./td[3]/text()').extract_first()
                fangst['fangst_fangst'] =  row.xpath('./td[4]/text()').extract_first()
                fangst['fangst_landingsdato'] =  row.xpath('./td[5]/text()').extract_first()

               
                #item['source'] = response.url
                #return kvote
                fangster.append(fangst)
        else:
            item['baat_har_fangster'] = "false"

        
        
        item['baat_fangster'] = fangster    

        #******************** End of adding fangster ****************************

        #item['fangst_fiskeslag'] =  response.xpath('//table[7]/tr/td[1]/text()').extract()
        #item['fangst_omraade'] =  response.xpath('//table[7]/tr/td[2]/text()').extract()
        #item['fangst_redskap'] =  response.xpath('//table[7]/tr/td[3]/text()').extract()
        #item['fangst_tonn'] =  response.xpath('//table[7]/tr/td[4]/text()').extract()
       
        

        return item

        


