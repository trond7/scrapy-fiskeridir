# -*- coding: utf-8 -*-
import scrapy
import pprint
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import re
from scrapy.shell import inspect_response
from scrapy.selector import Selector
from urlparse import urljoin
from fiskeridirektoratet.items import FiskekjoperItem


class FiskekjopereSpider(scrapy.Spider):
    name = "fiskekjopere"
    allowed_domains = ["fiskeridir.no"]
    start_urls = ['http://www.fiskeridir.no/register/kjoperreg']

    
    rules = (
       
        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(LinkExtractor(restrict_xpaths='//table[2]/tbody/tr/td/a'), follow=True, callback='parse'),)
    
	
    def parse(self, response):
    	print ('Filling out form')
        return scrapy.FormRequest.from_response(response,
                formdata={}, callback=self.parse_list)

    def parse_list(self, response):
    	#inspect_response(response, self)

    	#links = LinkExtractor(restrict_xpaths=("//table[2]/tbody/tr/td/a",))
    	#hxs = HtmlXPathSelector(response)
    	print ('Getting links')

    	links = response.selector.xpath('//table[2]//td[1]/a/@href').extract()
    	#print('trond')
    	#print links #pprint (links[1])
    	#print('trond2')

    	#SgmlLinkExtractor(restrict_xpaths=('//li[@class="next"]/a/@href',))
    	#pprint(links) 
    	i=0 
    	for link in links:
    		i+=1
    		#if link.has_attr('href'):
        	#print link
        	#print i
        	#if i<40:
        	if 'http://' not in link:
                          link = urljoin(response.url, link)
                          link = link.encode('WINDOWS_1252')
                          yield scrapy.Request(link,callback=self.parse_item)
        	
    		#pprint(link)

    def parse_item(self, response):
    	#inspect_response(response, self)
    	item = FiskekjoperItem()
    	item['orgnr'] =  response.xpath('//html/body/table[2]/tr[2]/td/table/tr[1]/td[2]/text()').extract() #//table[2]/tbody/tr[3]/td[1]
    	item['navn'] =  response.xpath('//tr[2]/td/table/tr[2]/td[2]/text()').extract() #tr[2]/td/table/tbody/tr[2]/td[2]/
    	item['ansvarlig'] =  response.xpath('//tr[2]/td/table/tr[3]/td[2]/text()').extract() 
    	item['leder'] =  response.xpath('//tr[2]/td/table/tr[4]/td[2]/text()').extract()
    	item['gateadresse'] =  response.xpath('//tr[4]/td/table/tr[1]/td[2]/text()').extract() 
    	item['postnr'] =  response.xpath('//tr[4]/td/table/tr[2]/td[2]/text()').extract() 
    	item['telefon'] =  response.xpath('//tr[4]/td/table/tr[3]/td[2]/text()').extract() 
    	item['telefax'] =  response.xpath('//tr[4]/td/table/tr[4]/td[2]/text()').extract() 
    	item['inndato'] =  response.xpath('//tr[6]/td/table/tr[2]/td[1]/text()').extract()
    	item['Utdato'] =  response.xpath('//tr[6]/td/table/tr[2]/td[2]/text()').extract()
    	item['oppdatert'] =  response.xpath('//tr[6]/td/table/tr[2]/td[3]/text()').extract()
    	item['status'] =  response.xpath('//tr[6]/td/table/tr[2]/td[4]/text()').extract()
    	item['statuskode'] =  response.xpath('//tr[6]/td/table/tr[2]/td[5]/text()').extract()
    	item['kommentar'] =  response.xpath('//tr[6]/td/table/tr[4]/td/text()').extract_first().replace('"', '') 
    	item['salgslag'] = response.xpath('//table[2]/tr[8]/td/table/tr/td/text()').extract_first().replace('"', '') 
    	item['source'] = response.url
 

    	return item

        pass


