# -*- coding: utf-8 -*-
import scrapy
import pprint
import pickle
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import re
from scrapy.shell import inspect_response
from scrapy.selector import Selector
from urlparse import urljoin
from fiskeridirektoratet.items import FangstItem


class FartoyfangstSpider(scrapy.Spider):
    name = "fartoyfangst"
    allowed_domains = ["fiskeridir.no"]
    start_urls = ['http://www.fiskeridir.no/register/fartoyreg/?m=frtykvote&f=1']

    def parse(self, response):
        print ('Filling out form')
        fylker = {'F':'Finnmark','T':'Troms','N':'Nordland','NT':'Nord-Trøndelag','ST':'Sør-Trøndelag','M':'Møre og Romsdal','SF':'Sogn og Fjordane','H':'Hordaland','R':'Rogaland','VA':'Vest-Agder','AA':'Aust-Agder','TK':'Telemark','V':'Vestfold','BD':'Buskerud','OP':'Oppland','HE':'Hedmark','O':'Oslo','A':'Akershus','Ø':'Østfold'}

        

        for key in fylker:
            print ('fetching fylke:', fylker[key])
            yield scrapy.FormRequest.from_response(response,
                formdata={'fylke': key}, callback=self.parse_list, meta={'fylke': fylker[key]})
                #pass

    def parse_list(self, response):
        #inspect_response(response, self)

       
        print ('Getting links')
        fylke = response.meta.get('fylke')
        links = response.selector.xpath('//table[2]//td[1]/a/@href').extract()
       

        i=0 
        for link in links:
            i+=1
            
            #print i
            #if i<5:
            if 'http://' not in link:
                          link = urljoin(response.url, link)
                          link = link.encode('WINDOWS_1252')
                          print(fylke,' : ',link)
                          yield scrapy.Request(link,callback=self.parse_item, meta={'fylke': fylke})
        pass    
            

    def parse_item(self, response):
    	items = []
        #inspect_response(response, self)
        
        regmerke = response.xpath('//table[2]/tr[2]/td[2]/text()').extract_first()

        rows = response.xpath('//table[7]//tr[position()>2]')
        #number_of_rows = response.xpath('count(//table[6]/tr[position()>2])').extract()
        #print str(regmerke) + " : " + number_of_rows
        i = 0
        for row in rows:
        	item = FangstItem()
        	#print row.xpath('td')
        	i += 1
        	istr = str(i)
        	item['baat_regmerke'] =  regmerke
        	item['fangst_id'] = regmerke+ "_" + istr
        	item['fangst_fiskeslag'] =  row.xpath('./td[1]/text()').extract()
        	item['fangst_omraade'] =  row.xpath('./td[2]/text()').extract()
        	item['fangst_redskap'] =  row.xpath('./td[3]/text()').extract()
        	item['fangst_tonn'] =  row.xpath('./td[4]/text()').extract()
        	item['fangst_landingsdato'] = row.xpath('./td[5]/text()').extract()
        	#item['source'] = response.url
        	items.append(item)
       
        return items

        
       
