# -*- coding: utf-8 -*-
import scrapy
import pprint
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import re
from scrapy.shell import inspect_response
from scrapy.selector import Selector
from urlparse import urljoin
from fiskeridirektoratet.items import FartoyregItem



class KommunerSpider(scrapy.Spider):
    name = "kommuner"
    allowed_domains = ["fiskeridir.no"]
    start_urls = ['http://www.fiskeridir.no/register/fartoyreg/?m=frtyoppl&f=1']

    def parse(self, response):
        print ('Filling out form')
        fylker = {'F':'Finnmark','T':'Troms','N':'Nordland','NT':'Nord-Trøndelag','ST':'Sør-Trøndelag','M':'Møre og Romsdal','SF':'Sogn og Fjordane','H':'Hordaland','R':'Rogaland','VA':'Vest-Agder','AA':'Aust-Agder','TK':'Telemark','V':'Vestfold','BD':'Buskerud','OP':'Oppland','HE':'Hedmark','O':'Oslo','A':'Akershus','Ø':'Østfold'}

        

        for key in fylker:
            print ('fetching fylke:', fylker[key])
            scrapy.FormRequest.from_response(response,
                formdata={'fylke': key})
                #pass
        

        kommuner = dict()
        inspect_response(response, self)

        liste = response.selector.xpath('//select[@name="kommune"]/option[position()>1]').extract()
                    #pprint.pprint(liste)
        for element in liste:
           	pprint.pprint(element)
           	kommuner[element.select.xpath('@value')] = element
        #pprint(kommuner)
