# -*- coding: utf-8 -*-
import scrapy
import pprint
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import re
from scrapy.shell import inspect_response
from scrapy.selector import Selector
from urlparse import urljoin
from fiskeridirektoratet.items import FiskerimantalletItem

class FiskerimantallSpider(scrapy.Spider):
    name = "fiskerimantall"
    allowed_domains = ["fiskeridir.no"]
    start_urls = ['http://www.fiskeridir.no/register/fiskermanntallet/?m=utl_mant']

    rules = (
       
        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(LinkExtractor(restrict_xpaths='//table[2]/tbody/tr/td/a'), follow=True, callback='parse'),)
    
	
    def parse(self, response):
    	print ('Filling out form')
        return scrapy.FormRequest.from_response(response,
                formdata={}, callback=self.parse_list)

    def parse_list(self, response):
    	#inspect_response(response, self)

    	#links = LinkExtractor(restrict_xpaths=("//table[2]/tbody/tr/td/a",))
    	#hxs = HtmlXPathSelector(response)
    	print ('Getting links')

    	links = response.selector.xpath('//table[2]//td[1]/a/@href').extract()
    	#print('trond')
    	#print links #pprint (links[1])
    	#print('trond2')

    	#SgmlLinkExtractor(restrict_xpaths=('//li[@class="next"]/a/@href',))
    	#pprint(links) 
    	i=0 
    	for link in links:
    		i+=1
    		#if link.has_attr('href'):
        	#print link
        	#print i
        	#if i<40:
        	if 'http://' not in link:
                          link = urljoin(response.url, link)
                          link = link.encode('WINDOWS_1252')
                          yield scrapy.Request(link,callback=self.parse_item)
        	
    		#pprint(link)

    def parse_item(self, response):
    	#inspect_response(response, self)
    	item = FiskerimantalletItem()
    	item['name'] =  response.xpath('//tr[3]/td[1]/text()').extract() #//table[2]/tbody/tr[3]/td[1]
    	item['fodt'] =  response.xpath('//tr[3]/td[2]/text()').extract() 
    	item['fylke'] =  response.xpath('//tr[7]/td[1]/text()').extract() 
    	item['kommune'] =  response.xpath('//tr[7]/td[2]/text()').extract()
    	item['inndato'] =  response.xpath('//tr[11]/td[1]/text()').extract() 
    	item['blad'] =  response.xpath('//tr[11]/td[2]/text()').extract() 
    	item['liste'] =  response.xpath('//tr[11]/td[3]/text()').extract() 
    	item['disp'] =  response.xpath('//tr[11]/td[4]/text()').extract() 
    	item['utdato'] =  response.xpath('//tr[11]/td[5]/text()').extract() 
    	item['aarsak'] =  response.xpath('//tr[11]/td[6]/text()').extract() 
    	item['source'] = response.url
 


    	print item['name']

    	return item

        pass

